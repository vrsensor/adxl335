
#include "ADXL335.h"


ADXL335::ADXL335()
{
	pinX_ = A0;
	pinY_ = A1;
	pinZ_ = A2;
	hIndex = 0;
	filtering = false;
}

ADXL335::ADXL335(int pinX, int pinY, int pinZ)
{
	pinX_ = pinX;
	pinY_ = pinY;
	pinZ_ = pinZ;
	hIndex = 0;
	filtering = false;
}

Vector3f ADXL335::getRawValues()
{
	return(Vector3f(analogRead(pinX_), analogRead(pinY_), analogRead(pinZ_)));
}

Vector3f ADXL335::getAccelValues()
{
	Vector3f v = raw2acceleration(getRawValues(), calibrationData);
	
	if(filtering)
	{
		// Calcul de la moyenne glissante
		hAccDataTotal = hAccDataTotal + v - hAccData[hIndex];
		hAccData[hIndex++] = v;
		hIndex %= ADXL355_HISTORY_DEPTH;
		v = hAccDataTotal / ADXL355_HISTORY_DEPTH;
	}
	
	return(v);
}

inline float ADXL335::raw2acceleration(float value, int bias)
{
	return((value - bias) * 0.1378);
}

inline Vector3f ADXL335::raw2acceleration(Vector3f value, Vector3f bias)
{
	return((value - bias) * 0.1378);
}


float ADXL335::getPitchAngle()
{
	Vector3f v = getAccelValues();
	return(atan2(v.x_, sqrtf(v.z_ * v.z_ + v.y_ * v.y_)));
}

float ADXL335::getPitchAngleDeg()
{
	return(getPitchAngle() * RAD2DEG);
}

float ADXL335::getRollAngle()
{
	Vector3f v = getAccelValues();
	return(atan2(v.y_, sqrtf(v.z_ * v.z_ + v.x_ * v.x_)));
}

float ADXL335::getRollAngleDeg()
{
	return(getRollAngle() * RAD2DEG);
}

void ADXL335::enableFiltering(bool status)
{
	filtering = status;
}

void ADXL335::setCalibrationData(Vector3f v)
{
	calibrationData = v;
	Serial.print("Calibration data = "); Serial.println(calibrationData.toString().c_str());
}

