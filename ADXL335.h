#ifndef __ADXL335__
#define __ADXL335__

#if ARDUINO < 100
#include <WProgram.h>
#else
#include <Arduino.h>
#endif
#include <math.h>
#include <Array.h>
#include <Vector3f.h>

#define RAD2DEG 57.29577951
#define DEG2RAD 0.017453292

/*!
 * \class ADXL335
 * 
 * \brief A class designed to manage an ADXL335 accelerometer connected
 * to the Arduino.
 * 
 * \author Charles Ingels
 * 
 * \date 2020.02.21
 * 
 */
class ADXL335
{
	static const uint8_t ADXL355_HISTORY_DEPTH = 30;
	
	public:
		/*! 
		 * \fn ADXL335()
		 * 
		 * \brief The constructor with default values for input pins.
		 * 
		 * The default input pins for reading the accelerations on the three
		 * axis are the following:
		 * - X axis => A0 pin
		 * - Y axis => A1 pin
		 * - Z axis => A2 pin
		 * 
		 */
		ADXL335();
		
		/*!
		 * \nf ADXL335(int pinX, int pinY, int pinZ)
		 *
		 * \brief The constructor to be used to specify the input pins to use.
		 * 
		 * With that constructor, it is possible to specify input pins other than 
		 * the default ones.
		 * 
		 * \param pinX	the input pin for X axis acceleration measurement
		 * \param pinY	the input pin for Y axis acceleration measurement
		 * \param pinZ	the input pin for Z axis acceleration measurement
		 *   
		 **/
		ADXL335(int pinX, int pinY, int pinZ);
		
		/*!
		 * \fn getRawValues()
		 * 
		 * \brief Return the three axis acceleration values.
		 * 
		 * With that function, it is possible to read the analog values that represent
		 * the accelerations over the three axis. Such values are in raw format with no
		 * processing.
		 * That function can be called at any time without synchronization constraints. That is
		 * at the time it is called, it will return the analog values given immediately by the internal ADC.
		 * 
		 * \return Returns a Vector3f object containing the raw values directly issued from the internal
		 * Arduino ADC representing the accelerations on the three axis. Those values are not transforemed
		 * into G values (1g vertically is 9.81 m/s2 for earth gravity)
		 * 
		 * 
		 * Here is an example of code snippet on how to use that function:
		 * \code
		 * Vector3f v;
		 * while(1) {
		 *   v = accelerometer.getRawValues();
		 *   
		 *   // v.x = acceleration on X axis
		 *   // v.y = acceleration on Y axis
		 *   // v.z = acceleration on Z axis
		 * 
		 *   sleep(10);
		 * }
		 * \endcode
		 */
		Vector3f getRawValues();
		
		/*!
		 * \fn getAccelValues()
		 * 
		 * \brief Return computed acceleration on all 3 axes 
		 * 
		 * Reads the acceleration values on all three axis and convert those values
		 * into G values. The vertical acceleration toward the earth is around 1g.
		 * 
		 * \return	Returns a Vector3f containing the computed values over the three axis
		 * 
		 */
		Vector3f getAccelValues();
		
		/*!
		 * \fn getPitchAngle()
		 * 
		 * \brief Return the pitch angle detected by the accelerometer.
		 * 
		 * That function reads the input accelerometer analog values and computes the pitch angle 
		 * against the gravity vector (vertical down vector). Such angle is the orientation against 
		 * the Y axis (left-right axis).
		 * 
		 * \return	Returns the pitch angle in radians.
		 * 
		 */
		float getPitchAngle();
		
		/*!
		 * \fn getPitchAngleDeg()
		 * 
		 * \brief Return the pitch angle detected by the accelerometer.
		 * 
		 * See the getPitchAngle() documentation for more details.
		 * 
		 * \return	Returns the pitch angle in degrees.
		 */
		float getPitchAngleDeg();
		
		/*!
		 * \fn getRollAngle()
		 * 
		 * \brief Return the roll angle detected by the accelerometer.
		 * 
		 * That function reads the input accelerometer analog values and computes the roll angle 
		 * against the gravity vector (vertical down vector). Such angle is the orientation against 
		 * the X axis (forward-backward axis).
		 * 
		 * \return	Returns the roll angle in radians.
		 * 
		 */
		float getRollAngle();
		
		/*!
		 * \fn getRollAngleDeg()
		 * 
		 * \brief Return the roll angle detected by the accelerometer.
		 * 
		 * See the getRollAngle() documentation for more details.
		 * 
		 * \return	Returns the roll angle in degrees.
		 */
		float getRollAngleDeg();
		
		// Enable filtering (low pass filter)
		/*!
		 * \fn enableFiltering(bool status)
		 * 
		 * \brief Enable/disable the accelerometer values filtering.
		 * 
		 * A low pass filter can be activated on the measured input accelerometer values which is
		 * helpful to smoothly filter them. The filtering is computed by applying a sliding average on the
		 * last 30 measured samples.
		 * By default the filtering is disabled.
		 * 
		 * \param
		 * status	true to enable filtering, false to disable it
		 *
		 */
		void enableFiltering(bool status);
		
		// Set the calibration offsets on the three axis (default is 0)
		/*!
		 * \fn setCalibrationData(Vector3f v)
		 * 
		 * \brief Set the calibration values for error compensation (or bias axis).
		 * 
		 * By default the accelerometer is not perfect and needs to have each of his axis value
		 * compensated by a factor axis dependent value.
		 * By default, those compensation factors are null, but if they are available then that
		 * function shall be used to specify them.
		 * When reading an axis accelerometer value (raw data), the bias is substracted to that value
		 * as an offset before being converted to G value.
		 * 
		 * The code snippet below shows what kind of compensation is done by the class:
		 * \code
		 * Vector3f bias; // the vector containing the bias values
		 * Vector3f v = (getRawValues() - bias) * RAW2G;
		 * // at the end, the "v" vector contains the compensated converted to G values
		 * \endcode
		 * 
		 * \param
		 * v	a Vector3f type value containing the compensation factor for each axis
		 * 
		 * \note
		 * Computing the bias can be done by using the ADXL335_Calibration Arduino program. Once those
		 * values are determined, the way to store them is up to the application (like being stored
		 * into the available EEPROM for example).
		 * 
		 */
		void setCalibrationData(Vector3f v);
		
		
	private:
		float raw2acceleration(float value, int bias);
		Vector3f raw2acceleration(Vector3f value, Vector3f bias);
		
		int pinX_, pinY_, pinZ_;
		bool filtering;
		
		Array<Vector3f, ADXL355_HISTORY_DEPTH> hAccData;
		Vector3f hAccDataTotal;
		uint8_t hIndex;
		
		Vector3f calibrationData;
};


#endif

