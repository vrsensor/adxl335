var searchData=
[
  ['getaccelvalues',['getAccelValues',['../class_a_d_x_l335.html#ad80280aa565fdd09188323dda45cc64e',1,'ADXL335']]],
  ['getpitchangle',['getPitchAngle',['../class_a_d_x_l335.html#abbe01db7c36a0810e3f98019029d13cb',1,'ADXL335']]],
  ['getpitchangledeg',['getPitchAngleDeg',['../class_a_d_x_l335.html#a6818b14bbeabba8bdded5f66de936282',1,'ADXL335']]],
  ['getrawvalues',['getRawValues',['../class_a_d_x_l335.html#a343382e175f1a5dcf58a5725b5d35bb5',1,'ADXL335']]],
  ['getrollangle',['getRollAngle',['../class_a_d_x_l335.html#ac02abceed86018e3dc10ea537cee33d4',1,'ADXL335']]],
  ['getrollangledeg',['getRollAngleDeg',['../class_a_d_x_l335.html#a5aad541a1030cd52a6d8c484e1877ed0',1,'ADXL335']]]
];
