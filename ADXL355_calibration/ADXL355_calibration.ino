#include <ADXL355_GY61.h>
#include <EEPROM.h>
#include <Vector3f.h>


#define PRINT(x) Serial.print(x)
#define PRINTLN(x) Serial.println(x)
#define PRINTFLOAT(x, d) Serial.print(x, d);

ADXL355_GY61 acc = ADXL355_GY61();

void print_delay(int t)
{
	long st = millis();
	
	while(t >= 0)
	{
		if(millis() - st >= 1000)
		{
			st = millis();
			
			PRINT(t);
			if(t > 0) PRINT("…");
			else PRINTLN("");
			
			t--;
		}
	}
}

/*
 * Setup function called once at startup
 */
void setup()
{
	Vector3f vec1, vec2, vec3;
	
	Serial.begin(500000);
	
	PRINTLN("-- Calibration accéléromètre ADXL355 --");
	PRINTLN("(pour une meilleure calibration, calez l'accéléromètre avec une équerre pour avoir des angles de 90°)");
	
	PRINT("1 - Posez l'accéléromètre à plat sans le bouger  ");
	print_delay(5);
	vec1 = acc.getRawValues();
	PRINTLN("");
	
	PRINT("2 - Inclinez l'accéléromètre de 90° vers l'avant sans le bouger  ");
	print_delay(5);
	vec2 = acc.getRawValues();
	PRINTLN("");
	
	PRINT("3 - Inclinez l'accéléromètre de 90° vers la droite sans le bouger  ");
	print_delay(5);
	vec3 = acc.getRawValues();
	PRINTLN("");
	
	Vector3f vec = Vector3f((vec1.x_ + vec3.x_) / 2, (vec1.y_ + vec2.y_) / 2, (vec2.z_ + vec3.z_) / 2);
	
	// Écriture des données de calibration en EEPROM
	EEPROM.put(0x10, vec.x_);
	EEPROM.put(0x14, vec.y_);
	EEPROM.put(0x18, vec.z_);
	
	vec = 0.0;
	
	// Relecture des données de calibration
	EEPROM.get(0x10, vec.x_);
	EEPROM.get(0x14, vec.y_);
	EEPROM.get(0x18, vec.z_);
	PRINT(vec.toString().c_str()); PRINTLN("");
	
	PRINTLN("Calibration terminée!");
	
}

void loop()
{
}
